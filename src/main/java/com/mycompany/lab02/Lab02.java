/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author chana
 */
public class Lab02 {

    static char[][] table = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printTable();
                printWin();
                printContinue();
                break;

            }
            if (isDraw()) {
                printTable();
                printDraw();
                printContinue();
                break;
            }
            switchPlayer();
        }
        inputContinue();
    }

    private static void printWelcome() {
        System.out.println("Welcome To OX Game !!!");

    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please in put a row number : ");
            row = sc.nextInt();
            System.out.print("Please in put a column number : ");
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '_') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }

        }

    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Win!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static void printContinue() {
        System.out.print("Would you like to play again? (y/n) : ");
    }

    private static void inputContinue() {
        Scanner sc = new Scanner(System.in);
        String newgame = sc.next();
        if (newgame.equals("y")) {
            resetTable();
            main(null);
        } else {
            System.out.println("Exit Game");
            System.exit(0);
        }
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '_') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println(currentPlayer + " Draw!!!");
    }

    private static void resetTable() {
        table = new char[][]{{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
        currentPlayer = 'X';
    }
}
